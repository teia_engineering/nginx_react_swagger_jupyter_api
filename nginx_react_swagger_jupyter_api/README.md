# NGINGX with React Swagger for Jupyter REST API
This little project is a way to generate a simple NGINX server that host a React app that creates a Swagger UI for a JSON file generated from Jupyter Kernel Gateway HTTP Mode. With Jupyter Kernel Gateway you can create a REST API end point utilzing a notebook.

## Setting up NGINX
We are using Ubuntu 20.04 on our systems and we are assuming you have already installed NGINX and configured it to automatically start on boot. If you dont [DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04) has a good tutorial on getting started. 

For the configurations of NGINX we have provided a file called `endpoints.conf` that shows the way to proxy the REST API through NGINX as well as the app. This file will go (and this is for Ubuntu) on `/etc/nginx/sites-available/`. Then we symlink to activate the config by doing `ln -s /etc/nginx/sites-available/endpoints.conf /etc/nginx/sites-enabled/`. More detailed information for this can be found in the DigitalOcean documentation. Once you put the file in the correct locations just restart NGINX by `systemctl restart nginx`.

NOTE: if the default page keeps appearing once you put the React code in the correct directories, just delete the default conf file from both sites-available and sites-enabled.

## Setting up the Python Environment
To set this environment you need to install `jupyter` and `jupyter_kernel_gateway`. Or you can just run `pip install -r requirements.txt`. 

Once that is done we can test the the kernel gateway. The following command will help you in getting it running with the test notebook we have provided. 

`jupyter kernelgateway --KernelGatewayApp.api='kernel_gateway.notebook_http' --KernelGatewayApp.seed_uri='react_swagger_notebook/endpoints.ipynb' --port=10101`

One thing to look at here is the last command, port. We have given it 10101 for default and this port is the same one you will see in the `endspoints.conf` file. If you decide to change the port be mindful to change it on the conf file as well.

Once the command is ran you can curl the endpoints to see if its available. (IMPORTANT: make sure curl is installed on your environment it will be needed on build of the React app.)

`curl "http://localhost:10101/api/convert?angle=180"`

or without port becasue of NGINX

`curl "http://localhost/api/convert?angle=180"`

## Setting up the React App
Since you have the project here just go into the directory and run `npm install`. Once that is done you can run the build command we have added to the package.json

`npm run build-with-swagger` 

or 

`npm run build`

The first one assumes you have the kernelgateway running on port 10101. (Yes this is set, you change the package.json if you change the port when initializing) The second command will take a previously generated `swagger.json` file in the repository and uses that. If you are not changing the rest endpoints is probably better to run the second one.