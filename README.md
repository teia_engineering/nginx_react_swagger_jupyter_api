# NGINGX with React Swagger for Jupyter REST API
This little project is a way to generate a simple NGINX server that host a React app that creates a Swagger UI for a JSON file generated from Jupyter Kernel Gateway HTTP Mode. With Jupyter Kernel Gateway you can create a REST API end point utilzing a notebook. You can run this as a Docker container or just as a NGINX server.

# No Docker Implmentation
If you clone the repo and go to the `nginx_react_swagger_jupyter_api` directory thre are instructions there on how to use the code and deploy it in an Ubuntu server.

# Docker Implmentation
If you would like to run the code as a docker container you have all the required `Dockerfile` per directory. The code is split in NGINX proxy (proxy directory), Jupyter APi (backend directory) and the React App (frontend).

In order to build the docker containers you need to create a network to be able to build them. Run the following command

`docker network create full_stack_network`

The name givne is the one used in the Dockerfiles. Afte this you can just run the following command and the containers get built and run.

`docker-compose up`

Once that finishes you can go to `localhost` and you will see the swagger interface built with ReactJS.